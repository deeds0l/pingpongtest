// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball2.h"
#include "Paddle.h"

// Sets default values
ABall2::ABall2()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ballMesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	RootComponent = ballMesh;
	ballMesh->SetSimulatePhysics(true);

	//OnActorBeginOverlap.AddDynamic(this, &Paddle::OnOverlap);
}

// Called when the game starts or when spawned
void ABall2::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABall2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

 /*void ABall2::OnOverlap_Implementation(AActor* OverlappedActor, AActor* OtherActor) {

	if (Cast<APaddle>(OtherActor) != nullptr)
	{
		Bounce();
	}
}

void ABall2::Bounce() {

}*/