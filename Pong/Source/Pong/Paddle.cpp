// Fill out your copyright notice in the Description page of Project Settings.


#include "Paddle.h"

// Sets default values
APaddle::APaddle()
{
	paddleMesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	RootComponent = paddleMesh;
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	paddleMesh->SetSimulatePhysics(true);
	movementForce = 100000;
}

// Called when the game starts or when spawned
void APaddle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APaddle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APaddle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis("MoveUp", this, &APaddle::MoveUp);
	InputComponent->BindAxis("MoveDown", this, &APaddle::MoveDown);
}

void APaddle::MoveUp(float Value) {
	FVector force = FVector(0,1,0) * movementForce * Value;
	paddleMesh->AddForce(force);
}

void APaddle::MoveDown(float Value) {
	FVector force = FVector(0, 1, 0) * movementForce * Value;
	paddleMesh->AddForce(force);
}

